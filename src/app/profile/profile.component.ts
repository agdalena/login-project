import { Component, OnInit } from '@angular/core';
import {LoginComponent} from '../login/login.component';
import { Login } from '../login';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  username = localStorage.getItem('currentUser');

  constructor(private router: Router) {}

  ngOnInit() {
    if (!localStorage.getItem('currentUser')) {
      this.router.navigate(['login']);
    }
  }

  onSubmit() {
    localStorage.removeItem('currentUser');
    this.router.navigate(['login']);
  }

}
