import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Data } from '../app/data';

@Injectable()
export class AuthenticationService {

  constructor(
    private http: HttpClient) {}

  model: any = {};
  LOADING_TIME = 500;

  login(username: string, password: string) {


    let myFirstPromise = new Promise((resolve, reject) => {
        setTimeout(function(){
          if (username === Data.USERNAME1 && password === Data.PASSWORD1 ||
            username === Data.USERNAME2 && password === Data.PASSWORD2 ) {
              resolve(true);
        }
          else {
            reject(false); }
        }, this.LOADING_TIME);
      });

      return myFirstPromise;
}
}

