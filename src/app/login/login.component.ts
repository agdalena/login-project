import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {AuthenticationService} from '../authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: any = {};
  returnUrl: string;
  loading = false;
  error: string;
  errorRequiredUsername: string;
  errorRequiredPassword: string;
  

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService) { }

    ngOnInit() {
      if (localStorage.getItem('currentUser')) {
        this.router.navigate(['profile']);
      }
  }

  login() {
    this.loading = true;
    if (!this.model.username || !this.model.password) {
        if (!this.model.username) {
          this.loading = false;
          this.error = '';
          this.errorRequiredUsername = 'Username is required';
          if (!this.model.password) {
            this.loading = false;
            this.error = '';
            this.errorRequiredPassword = 'Password is required';
          }
  
        }
        else if (!this.model.password) {
          this.loading = false;
          this.error = '';
          this.errorRequiredPassword = 'Password is required';
        }

    }
    else if (localStorage.getItem('currentUser')) {
      this.router.navigate(['profile']);
    }
    else {
      this.authenticationService.login(this.model.username, this.model.password).then((successMessage) => {
        localStorage.setItem('currentUser', this.model.username);
        this.router.navigate(['profile']);
      })
      .catch(
        (reason) => {
          this.loading = false;
          this.errorRequiredPassword = '';
          this.errorRequiredUsername = '';
          this.error = 'Wrong username or password!';
          console.log(this.model.username, this.model.password);
        });
    }
  }
}

